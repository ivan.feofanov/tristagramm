package auth

import (
	"fmt"

	"golang.org/x/crypto/bcrypt"
)

func GenerateHash(password string) (string, error) {
	hashedBytes, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return "", fmt.Errorf("%w", err)
	}

	hash := string(hashedBytes)

	return hash, nil
}

func ComparePassword(hash string, password string) error {
	incoming := []byte(password)
	existing := []byte(hash)

	if err := bcrypt.CompareHashAndPassword(existing, incoming); err != nil {
		return fmt.Errorf("%w", err)
	}

	return nil
}
