package auth

import (
	"errors"
	"fmt"
)

var ErrInvalidToken = errors.New("token is invalid")

type UnexpectedSigningMethodError struct {
	Algorithm interface{}
}

func (e UnexpectedSigningMethodError) Error() string {
	return fmt.Sprintf("unexpected signing method: %v", e.Algorithm)
}
