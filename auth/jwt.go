// Package auth provides auth functional for the REST API
package auth

import (
	"fmt"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
	"github.com/google/uuid"
)

type ClaimsWithUserID struct {
	UserID uuid.UUID
	jwt.StandardClaims
}

const YEAR = 60 * 24 * 365 // Year in minutes

func CreateToken(userID uuid.UUID) (string, error) {
	var err error

	// Create the Claims
	claims := ClaimsWithUserID{
		userID,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Minute * YEAR).Unix(),
			Issuer:    "Tristagramm LLC.",
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, &claims)

	// Sign and get the complete encoded token as a string using the secret
	tokenString, err := token.SignedString([]byte(os.Getenv("ACCESS_SECRET")))
	if err != nil {
		return "", fmt.Errorf("%w", err)
	}

	return tokenString, nil
}

func ExtractToken(r *http.Request) string {
	token := r.Header.Get("Authorization")
	if strings.HasPrefix(token, "Bearer ") {
		token = strings.TrimPrefix(token, "Bearer ")

		return token
	}

	return ""
}

func DecodeToken(r *http.Request) (*jwt.Token, error) {
	tokenString := ExtractToken(r)
	token, err := jwt.ParseWithClaims(tokenString, &ClaimsWithUserID{}, func(token *jwt.Token) (interface{}, error) {
		// Make sure that the token method conform to "SigningMethodHMAC"
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, UnexpectedSigningMethodError{Algorithm: token.Header["alg"]}
		}

		return []byte(os.Getenv("ACCESS_SECRET")), nil
	})

	if err != nil {
		return nil, fmt.Errorf("%w", err)
	}

	return token, nil
}

func ExtractUserID(r *http.Request) (*uuid.UUID, error) {
	token, err := DecodeToken(r)
	if err != nil {
		return nil, err
	}

	if claims, ok := token.Claims.(*ClaimsWithUserID); ok && token.Valid {
		return &claims.UserID, nil
	}

	return nil, err
}

func TokenValid(r *http.Request) error {
	token, err := DecodeToken(r)
	if err != nil {
		return err
	}

	if !token.Valid {
		return ErrInvalidToken
	}

	return nil
}

func TokenAuthMiddleware() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		err := TokenValid(ctx.Request)
		if err != nil {
			ctx.JSON(http.StatusUnauthorized, err.Error())
			ctx.Abort()

			return
		}

		userID, err := ExtractUserID(ctx.Request)
		if err != nil {
			ctx.JSON(http.StatusInternalServerError, err.Error())
		}

		ctx.Set("userID", userID.String())
		ctx.Next()
	}
}
