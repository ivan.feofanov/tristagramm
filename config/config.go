// Package config provides getting configuration from env using GetConfig function
package config

import (
	"sync"

	"github.com/cristalhq/aconfig"
	"github.com/cristalhq/aconfig/aconfigdotenv"
)

type Config struct {
	Environment string `default:"development"`
	UploadDir   string `default:"upload"`
	REST        struct {
		Host    string `default:"127.0.0.1"`
		Port    int    `default:"8080"`
		GinMode string `default:"debug"`
	}
	GRPC struct {
		Host string `default:"127.0.0.1"`
		Port int    `default:"8008"`
	}
	DB struct {
		Host     string
		Port     string
		Name     string
		Username string
		Password string
	}
}

func GetConfig() *Config {
	var (
		cfg  Config
		once sync.Once
	)

	once.Do(func() {
		loader := aconfig.LoaderFor(&cfg, aconfig.Config{
			Files:              []string{".env"},
			AllowUnknownFields: true,
			AllowUnknownEnvs:   true,
			AllFieldRequired:   true,
			FileDecoders: map[string]aconfig.FileDecoder{
				".env": aconfigdotenv.New(),
			},
		})

		if err := loader.Load(); err != nil {
			panic(err)
		}
	})

	return &cfg
}
