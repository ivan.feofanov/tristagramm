// Package api is responsive for creating REST API for service
package api

import (
	"errors"
	"fmt"
	"log"
	"mime/multipart"
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/ivan.feofanov/tristagramm/config"
	"gitlab.com/ivan.feofanov/tristagramm/repository"
)

type NewPost struct {
	Text  string                  `form:"text" binding:"required"`
	Files []*multipart.FileHeader `form:"files" binding:"required"`
}

func getAllPostsHandler(ctx *gin.Context, repo *repository.Repository) {
	userID := ctx.GetString("userID")
	allPosts, err := repo.Posts.GetAll(userID)

	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err.Error())

		return
	}

	ctx.JSON(http.StatusOK, allPosts)
}

func getPostHandler(ctx *gin.Context, repo *repository.Repository) {
	post, err := repo.Posts.Get(ctx.GetString("userID"), ctx.Param("postID"))
	if errors.Is(err, repository.ErrRecordNotFound) {
		ctx.String(http.StatusNotFound, "post not found")

		return
	} else if err != nil {
		ctx.String(http.StatusInternalServerError, err.Error())

		return
	}

	ctx.JSON(http.StatusOK, post)
}

func createPostHandler(ctx *gin.Context, repo *repository.Repository, cfg *config.Config) {
	var newPost NewPost
	if err := ctx.ShouldBind(&newPost); err != nil {
		ctx.String(http.StatusBadRequest, err.Error())

		return
	}

	images := []string{}

	for _, file := range newPost.Files {
		filePath := fmt.Sprintf("%s/%s", cfg.UploadDir, file.Filename)

		err := ctx.SaveUploadedFile(file, filePath)
		if err != nil {
			log.Fatal(err)
		}

		images = append(images, filePath)
	}

	post, err := repo.Posts.Create(ctx.GetString("userID"), newPost.Text, images)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err.Error())

		return
	}

	ctx.JSON(http.StatusOK, post)
}

func deletePostHandler(ctx *gin.Context, repo *repository.Repository) {
	err := repo.Posts.Delete(ctx.GetString("userID"), ctx.Param("postID"))
	if errors.Is(err, repository.ErrRecordNotFound) {
		ctx.JSON(http.StatusNotFound, err.Error())

		return
	} else if err != nil {
		ctx.JSON(http.StatusInternalServerError, err.Error())

		return
	}

	ctx.Status(http.StatusOK)
}

func ConnectPostRoutes(router *gin.RouterGroup, repo *repository.Repository, cfg *config.Config) {
	postsAPI := router.Group("/posts")

	postsAPI.GET("/", func(ctx *gin.Context) {
		getAllPostsHandler(ctx, repo)
	})

	postsAPI.GET("/:postID", func(ctx *gin.Context) {
		getPostHandler(ctx, repo)
	})

	postsAPI.POST("/", func(ctx *gin.Context) {
		createPostHandler(ctx, repo, cfg)
	})

	postsAPI.DELETE("/:postID", func(ctx *gin.Context) {
		deletePostHandler(ctx, repo)
	})
}
