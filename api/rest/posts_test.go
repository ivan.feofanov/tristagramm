package api_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	gofakeit "github.com/brianvoe/gofakeit/v6"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"

	api "gitlab.com/ivan.feofanov/tristagramm/api/rest"
	"gitlab.com/ivan.feofanov/tristagramm/auth"
	"gitlab.com/ivan.feofanov/tristagramm/config"
	"gitlab.com/ivan.feofanov/tristagramm/database"
	"gitlab.com/ivan.feofanov/tristagramm/repository"
)

func CreateFormDataBody(values map[string]interface{}) (*bytes.Buffer, string) {
	// Prepare a form that you will submit to that URL.
	var buffer bytes.Buffer
	writer := multipart.NewWriter(&buffer)

	for key, value := range values {
		var (
			formWriter io.Writer
			err        error
		)
		// Add a file
		if fileData, ok := value.(*bytes.Buffer); ok {
			if formWriter, err = writer.CreateFormFile(key, "test.txt"); err != nil {
				log.Fatal(err)
			}

			if _, err = io.Copy(formWriter, fileData); err != nil {
				log.Fatal(err)
			}

			continue
		}
		// Add other fields
		if formWriter, err = writer.CreateFormField(key); err != nil {
			log.Fatal(err)
		}

		if _, err = io.Copy(formWriter, strings.NewReader(fmt.Sprint(value))); err != nil {
			log.Fatal(err)
		}
	}

	if err := writer.Close(); err != nil {
		log.Fatal(err)
	}

	return &buffer, writer.FormDataContentType()
}

func PerformRequest( //nolint: revive
	handler http.Handler, method, path string, body io.Reader, contentType, token *string) *httptest.ResponseRecorder {
	if contentType == nil {
		ct := "application/json;"
		contentType = &ct
	}

	req := httptest.NewRequest(method, path, body)
	if token != nil {
		req.Header.Add("Authorization", "Bearer "+*token)
	}

	req.Header.Add("Content-Type", *contentType)

	responseRecorder := httptest.NewRecorder()
	handler.ServeHTTP(responseRecorder, req)

	return responseRecorder
}

type MockedPostsRepo struct {
	mock.Mock
}

func (m *MockedPostsRepo) GetAll(userID string) ([]database.Post, error) {
	args := m.Called(userID)
	if posts, ok := args[0].([]database.Post); ok {
		return posts, nil
	}

	return nil, fmt.Errorf("%w", args.Error(1))
}

func (m *MockedPostsRepo) Get(userID, postID string) (*database.Post, error) {
	args := m.Called(userID, postID)
	if post, ok := args[0].(*database.Post); ok {
		return post, nil
	}

	return nil, fmt.Errorf("%w", args.Error(1))
}

func (m *MockedPostsRepo) Create(userID string, text string, images []string) (*database.Post, error) {
	args := m.Called(userID, text, images)
	if post, ok := args[0].(*database.Post); ok {
		return post, nil
	}

	return nil, fmt.Errorf("%w", args.Error(1))
}

func (m *MockedPostsRepo) Delete(userID, postID string) error {
	m.Called(userID, postID)

	return nil
}

type PostsTestSuite struct {
	suite.Suite
	userID          uuid.UUID
	token           string
	mockedPostsRepo MockedPostsRepo
	router          *gin.Engine
}

func (suite *PostsTestSuite) SetupTest() {
	gin.SetMode(gin.TestMode)

	err := os.Setenv("UPLOAD_DIR", suite.T().TempDir())
	suite.NoError(err)

	gofakeit.Seed(0)

	suite.userID = uuid.New()
	suite.token, _ = auth.CreateToken(suite.userID)
	suite.mockedPostsRepo = MockedPostsRepo{} //nolint: exhaustivestruct
	suite.router = gin.New()
	suite.router.Use(auth.TokenAuthMiddleware())
}

func (suite *PostsTestSuite) TestGetPosts() {
	var post database.Post
	err := gofakeit.Struct(&post)
	suite.NoError(err)
	assertedBody, err := json.Marshal(&[]database.Post{post})
	suite.NoError(err)

	suite.mockedPostsRepo.
		On("GetAll", suite.userID.String()).
		Return([]database.Post{post}, nil)

	repo := repository.Repository{Posts: &suite.mockedPostsRepo} //nolint: exhaustivestruct

	api.ConnectPostRoutes(suite.router.Group(""), &repo, &config.Config{})

	responseRecorder := PerformRequest(
		suite.router,
		"GET",
		"/posts/",
		nil,
		nil,
		&suite.token)
	suite.Equal(http.StatusOK, responseRecorder.Code)
	suite.Equal(assertedBody, responseRecorder.Body.Bytes())
}

func (suite *PostsTestSuite) TestGetPost() {
	var post database.Post
	err := gofakeit.Struct(&post)
	suite.NoError(err)
	assertedBody, err := json.Marshal(&post)
	suite.NoError(err)

	suite.mockedPostsRepo.
		On("Get", suite.userID.String(), post.UID.String()).
		Return(&post, nil)

	repo := repository.Repository{Posts: &suite.mockedPostsRepo} //nolint: exhaustivestruct

	api.ConnectPostRoutes(suite.router.Group(""), &repo, &config.Config{})

	responseRecorder := PerformRequest(
		suite.router,
		"GET",
		fmt.Sprintf("/posts/%s", post.UID.String()),
		nil,
		nil, &suite.token)
	suite.Equal(http.StatusOK, responseRecorder.Code)
	suite.Equal(assertedBody, responseRecorder.Body.Bytes())
}

func (suite *PostsTestSuite) TestCreatePost() {
	var post database.Post
	err := gofakeit.Struct(&post)
	suite.NoError(err)

	assertedBody, err := json.Marshal(&post)
	suite.NoError(err)

	file := bytes.NewBufferString("hello\n")
	tmpDir := suite.T().TempDir()
	requestJSON := map[string]interface{}{"text": post.Text, "files": file}
	body, contentType := CreateFormDataBody(requestJSON)

	suite.mockedPostsRepo.
		On("Create", suite.userID.String(), post.Text, []string{fmt.Sprintf("%s/test.txt", tmpDir)}).
		Return(&post, nil)

	repo := repository.Repository{Posts: &suite.mockedPostsRepo} //nolint: exhaustivestruct

	api.ConnectPostRoutes(suite.router.Group(""), &repo, &config.Config{UploadDir: tmpDir})

	responseRecorder := PerformRequest(
		suite.router,
		"POST",
		"/posts/",
		body,
		&contentType,
		&suite.token)

	suite.Equal(http.StatusOK, responseRecorder.Code)
	suite.Equal(assertedBody, responseRecorder.Body.Bytes())
}

func (suite *PostsTestSuite) TestDeletePost() {
	postUID := gofakeit.UUID()

	suite.mockedPostsRepo.
		On("Delete", suite.userID.String(), postUID).
		Return(nil)

	repo := repository.Repository{Posts: &suite.mockedPostsRepo} //nolint: exhaustivestruct

	api.ConnectPostRoutes(suite.router.Group(""), &repo, &config.Config{})

	responseRecorder := PerformRequest(
		suite.router,
		"DELETE",
		fmt.Sprintf("/posts/%s", postUID),
		nil,
		nil,
		&suite.token)

	suite.Equal(http.StatusOK, responseRecorder.Code)
}

func TestPostsTestSuite(t *testing.T) {
	t.Parallel()
	suite.Run(t, new(PostsTestSuite))
}
