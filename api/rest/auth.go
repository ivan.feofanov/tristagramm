package api

import (
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"

	"gitlab.com/ivan.feofanov/tristagramm/auth"
	"gitlab.com/ivan.feofanov/tristagramm/repository"
)

type Credentials struct {
	Email    string
	Password string
}

func Login(ctx *gin.Context, repo *repository.Repository) {
	var credentials Credentials
	if err := ctx.ShouldBind(&credentials); err != nil {
		ctx.JSON(http.StatusUnprocessableEntity, "Invalid data provided")

		return
	}

	user, err := repo.Users.Get(credentials.Email)
	if errors.Is(err, repository.ErrRecordNotFound) {
		ctx.String(http.StatusNotFound, "user not found")

		return
	}

	if err = auth.ComparePassword(user.Password, credentials.Password); err != nil {
		ctx.JSON(http.StatusUnauthorized, "Please provide valid login details")

		return
	}

	token, err := auth.CreateToken(user.UID)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err.Error())

		return
	}

	ctx.JSON(http.StatusOK, map[string]string{"token": token})
}

func Register(ctx *gin.Context, repo *repository.Repository) {
	var credentials Credentials
	if err := ctx.ShouldBind(&credentials); err != nil {
		ctx.JSON(http.StatusUnprocessableEntity, err.Error())

		return
	}

	user, err := repo.Users.Get(credentials.Email)
	if err != nil && !errors.Is(err, repository.ErrRecordNotFound) {
		ctx.JSON(http.StatusInternalServerError, err.Error())

		return
	}

	if user != nil {
		ctx.String(http.StatusBadRequest, "user already exist")

		return
	}

	user, err = repo.Users.Create(credentials.Email, credentials.Password)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, err.Error())

		return
	}

	token, err := auth.CreateToken(user.UID)
	if err != nil {
		ctx.JSON(http.StatusUnprocessableEntity, err.Error())

		return
	}

	ctx.JSON(http.StatusOK, map[string]string{"token": token})
}

func ConnectAuthRoutes(router *gin.Engine, repo *repository.Repository) {
	router.POST("/login", func(ctx *gin.Context) {
		Login(ctx, repo)
	})
	router.POST("/register", func(ctx *gin.Context) {
		Register(ctx, repo)
	})
}
