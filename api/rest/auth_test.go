package api_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"testing"

	"github.com/brianvoe/gofakeit/v6"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"

	api "gitlab.com/ivan.feofanov/tristagramm/api/rest"
	"gitlab.com/ivan.feofanov/tristagramm/auth"
	"gitlab.com/ivan.feofanov/tristagramm/database"
	"gitlab.com/ivan.feofanov/tristagramm/repository"
)

func CreateJSONBody(values map[string]interface{}) (*bytes.Buffer, string) {
	JSONData, err := json.Marshal(values)
	if err != nil {
		panic(err)
	}

	return bytes.NewBuffer(JSONData), "application/json;"
}

type MockedUsersRepo struct {
	mock.Mock
}

func (m *MockedUsersRepo) Get(email string) (*database.User, error) {
	args := m.Called(email)
	if user, ok := args[0].(*database.User); ok {
		return user, nil
	}

	return nil, fmt.Errorf("%w", args.Error(1))
}
func (m *MockedUsersRepo) Create(email, password string) (*database.User, error) {
	args := m.Called(email, password)
	if user, ok := args[0].(database.User); ok {
		return &user, nil
	}

	return nil, fmt.Errorf("%w", args.Error(1))
}

type AuthTestSuite struct {
	suite.Suite
	mockedUsersRepo MockedUsersRepo
	router          *gin.Engine
}

func (suite *AuthTestSuite) SetupTest() {
	gin.SetMode(gin.TestMode)

	gofakeit.Seed(0)

	suite.mockedUsersRepo = MockedUsersRepo{} //nolint: exhaustivestruct
	suite.router = gin.New()
}

func (suite *AuthTestSuite) TestLogin() {
	var user *database.User
	err := gofakeit.Struct(&user)
	suite.NoError(err)
	token, err := auth.CreateToken(user.UID)
	suite.NoError(err)

	assertedBody, _ := CreateJSONBody(map[string]interface{}{"token": token})

	postBody, contentType := CreateJSONBody(map[string]interface{}{
		"email":    user.Email,
		"password": user.Password,
	})

	user.Password, err = auth.GenerateHash(user.Password)
	suite.NoError(err)

	suite.mockedUsersRepo.On("Get", user.Email).Return(user, nil)

	repo := repository.Repository{Users: &suite.mockedUsersRepo} //nolint: exhaustivestruct
	api.ConnectAuthRoutes(suite.router, &repo)

	responseRecorder := PerformRequest(
		suite.router,
		"POST",
		"/login",
		postBody,
		&contentType,
		nil,
	)

	suite.Equal(http.StatusOK, responseRecorder.Code)
	suite.Equal(assertedBody, responseRecorder.Body)
}

func (suite *AuthTestSuite) TestRegister() {
	var user *database.User
	err := gofakeit.Struct(&user)
	suite.NoError(err)
	token, err := auth.CreateToken(user.UID)
	suite.NoError(err)

	assertedBody, _ := CreateJSONBody(map[string]interface{}{"token": token})

	postBody, contentType := CreateJSONBody(map[string]interface{}{
		"email":    user.Email,
		"password": user.Password,
	})

	suite.mockedUsersRepo.On("Get", user.Email).Return(nil, repository.ErrRecordNotFound)
	suite.mockedUsersRepo.On("Create", user.Email, user.Password).Return(*user, nil)

	repo := repository.Repository{Users: &suite.mockedUsersRepo} //nolint: exhaustivestruct
	api.ConnectAuthRoutes(suite.router, &repo)

	responseRecorder := PerformRequest(
		suite.router,
		"POST",
		"/register",
		postBody,
		&contentType,
		nil,
	)

	suite.Equal(http.StatusOK, responseRecorder.Code)
	suite.Equal(assertedBody, responseRecorder.Body)
}

func TestAuthTestSuite(t *testing.T) {
	t.Parallel()
	suite.Run(t, new(AuthTestSuite))
}
