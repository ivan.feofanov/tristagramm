package api

import (
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"

	"gitlab.com/ivan.feofanov/tristagramm/auth"
	"gitlab.com/ivan.feofanov/tristagramm/config"
	"gitlab.com/ivan.feofanov/tristagramm/repository"
)

func SetupRouter(repo *repository.Repository, cfg *config.Config) *gin.Engine {
	gin.SetMode(cfg.REST.GinMode)

	router := gin.Default()
	router.Static("/upload", "./upload")

	authorized := router.Group("/")
	authorized.Use(auth.TokenAuthMiddleware())

	v1 := authorized.Group("/v1")

	ConnectAuthRoutes(router, repo)
	ConnectPostRoutes(v1, repo, cfg)

	return router
}

const readHeaderTimeoutSec = 5 // in seconds

func CreateRestSrv(repo *repository.Repository, cfg *config.Config) *http.Server {
	return &http.Server{
		Addr:              fmt.Sprintf("%s:%d", cfg.REST.Host, cfg.REST.Port),
		Handler:           SetupRouter(repo, cfg),
		ReadHeaderTimeout: readHeaderTimeoutSec * time.Second,
	}
}
