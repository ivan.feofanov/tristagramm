# syntax=docker/dockerfile:1

##
## Build
##
FROM golang:alpine as builder
RUN apk add --no-cache --update git

WORKDIR /app

COPY go.mod ./
COPY go.sum ./

RUN go mod download

ADD . ./

RUN CGO_ENABLED=0 GOOS=linux go build -a -o app .

##
## Deploy
##
FROM alpine

RUN addgroup -S nemo && adduser -S nemo -G nemo
USER nemo:nemo

COPY --from=builder /app/app /bin/

ENTRYPOINT ["/bin/app"]
