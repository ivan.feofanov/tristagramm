package main

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/sourcegraph/conc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	grpcapi "gitlab.com/ivan.feofanov/tristagramm/api/grpc"
	restapi "gitlab.com/ivan.feofanov/tristagramm/api/rest"
	"gitlab.com/ivan.feofanov/tristagramm/config"
	"gitlab.com/ivan.feofanov/tristagramm/repository"
)

const TimeToShutdown = 5 * time.Second

type Server struct {
	Rest *http.Server
	GRPC *grpc.Server
}

func GracefulShutdown(ctx context.Context, srv *Server) error {
	log.Print("Shutting down REST API and gRPC servers")

	// copied from official Gin documentation (https://github.com/gin-gonic/gin#graceful-shutdown-or-restart)
	// The context is used to inform the server it has 5 seconds to finish
	// the request it is currently handling
	closingCtx, cancel := context.WithTimeout(ctx, TimeToShutdown)
	defer cancel()

	if err := srv.Rest.Shutdown(closingCtx); err != nil {
		return fmt.Errorf("shutdown error: %w", err)
	}

	log.Print("REST API server down")

	srv.GRPC.GracefulStop()
	log.Print("gRPC server down")

	return nil
}

func main() {
	cfg := config.GetConfig()

	repo := repository.NewRepository(cfg)
	srv := Server{Rest: restapi.CreateRestSrv(repo, cfg), GRPC: grpc.NewServer()}
	ctx := context.Background()

	var wg conc.WaitGroup

	// REST API section
	wg.Go(func() {
		// If the server is closing during to intended shutdown we don't need to throw an error
		if err := srv.Rest.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			panic(err)
		}
	})

	// gRPC section
	wg.Go(func() {
		listener, err := net.Listen("tcp", fmt.Sprintf("%s:%d", cfg.GRPC.Host, cfg.GRPC.Port))
		if err != nil {
			panic(err)
		}
		reflection.Register(srv.GRPC)
		grpcapi.RegisterTristagrammServer(srv.GRPC, &grpcapi.Server{Repo: repo})
		if err = srv.GRPC.Serve(listener); err != nil {
			panic(err)
		}
	})

	// Graceful shutdown if server stopped by system
	notifyContext, stop := signal.NotifyContext(ctx, os.Interrupt, syscall.SIGTERM)

	wg.Go(func() {
		for {
			select {
			case <-ctx.Done():
			case <-notifyContext.Done():
				if err := GracefulShutdown(ctx, &srv); err != nil {
					panic(err)
				}

				return
			}
		}
	})

	wg.Wait()

	log.Print("Good night!")

	stop()
}
