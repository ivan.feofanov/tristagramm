install:
    go mod download

build: install
    go build -a -o dist/app .

test: install
    go test ./...

run: install
    go run main.go
