package repository

import (
	"errors"
	"fmt"

	"gorm.io/gorm"

	"gitlab.com/ivan.feofanov/tristagramm/auth"
	"gitlab.com/ivan.feofanov/tristagramm/database"
)

type UsersRepoInterface interface {
	Get(email string) (*database.User, error)
	Create(email, password string) (*database.User, error)
}

type UsersRepository struct {
	BaseRepo
}

func (r *UsersRepository) Get(email string) (*database.User, error) {
	var user database.User

	tx := r.DB.Where("email = ?", email).Take(&user)
	if errors.Is(tx.Error, gorm.ErrRecordNotFound) {
		return nil, ErrRecordNotFound
	} else if tx.Error != nil {
		return nil, tx.Error
	}

	return &user, nil
}

func (r *UsersRepository) Create(email, password string) (*database.User, error) {
	hashedPassword, err := auth.GenerateHash(password)
	if err != nil {
		return nil, fmt.Errorf("%w", err)
	}

	user := database.User{Email: email, Password: hashedPassword}

	if result := r.DB.Create(&user); result.Error != nil {
		return nil, fmt.Errorf("%w", err)
	}

	return &user, nil
}
