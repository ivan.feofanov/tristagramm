package repository

import (
	"errors"
	"fmt"

	"github.com/google/uuid"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"

	"gitlab.com/ivan.feofanov/tristagramm/database"
)

type PostsRepository struct {
	BaseRepo
}

type PostsRepoInterface interface {
	GetAll(userID string) ([]database.Post, error)
	Get(userID, postID string) (*database.Post, error)
	Create(userID string, text string, images []string) (*database.Post, error)
	Delete(userID, postID string) error
}

func (r *PostsRepository) GetAll(userID string) ([]database.Post, error) {
	var allPosts []database.Post
	query := r.DB.
		Preload(clause.Associations).
		Order("updated_at desc").
		Where("user_id = ?", userID).
		Find(&allPosts)

	if query.Error != nil {
		return nil, query.Error
	}

	return allPosts, nil
}

func (r *PostsRepository) Get(userID, postID string) (*database.Post, error) {
	var post database.Post

	tx := r.DB.Preload(clause.Associations).Where("user_id = ?", userID).Take(&post, postID)
	if errors.Is(tx.Error, gorm.ErrRecordNotFound) {
		return nil, ErrRecordNotFound
	} else if tx.Error != nil {
		return nil, tx.Error
	}

	return &post, nil
}

func (r *PostsRepository) Create(userID string, text string, images []string) (*database.Post, error) {
	dbImages := []database.Image{}
	for _, imagePath := range images {
		dbImages = append(dbImages, database.Image{Path: imagePath})
	}

	userUID, err := uuid.Parse(userID)
	if err != nil {
		return nil, fmt.Errorf("%w", err)
	}

	post := database.Post{UserID: userUID, Text: text, Images: dbImages}
	if tx := r.DB.Create(&post); tx.Error != nil {
		return nil, tx.Error
	}

	return &post, nil
}

func (r *PostsRepository) Delete(userID, postID string) error {
	tx := r.DB.Debug().Where("user_id = ?", userID).Delete(&database.Post{}, "uid = ?", postID)
	if tx.Error != nil {
		return tx.Error
	} else if tx.RowsAffected == 0 {
		return ErrRecordNotFound
	}

	return nil
}
