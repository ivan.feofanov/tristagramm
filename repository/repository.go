// Package repository provides data access layer according to repository pattern
package repository

import (
	"gorm.io/gorm"

	"gitlab.com/ivan.feofanov/tristagramm/config"
	"gitlab.com/ivan.feofanov/tristagramm/database"
)

type BaseRepo struct {
	DB *gorm.DB
}

type Repository struct {
	Users UsersRepoInterface
	Posts PostsRepoInterface
}

func NewRepository(cfg *config.Config) *Repository {
	db := database.SetupDatabase(cfg)

	return &Repository{
		Users: &UsersRepository{BaseRepo{DB: db}},
		Posts: &PostsRepository{BaseRepo{DB: db}},
	}
}
