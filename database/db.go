// Package database provides database access
package database

import (
	"fmt"
	"log"

	_ "github.com/lib/pq" // import PostgreSQL driver
	"gorm.io/driver/postgres"
	"gorm.io/gorm"

	"gitlab.com/ivan.feofanov/tristagramm/config"
)

func MakeDSN(cfg *config.Config) string {
	return fmt.Sprintf(
		"host=%s port=%s dbname=%s user=%s password=%s sslmode=disable",
		cfg.DB.Host, cfg.DB.Port, cfg.DB.Name, cfg.DB.Username, cfg.DB.Password,
	)
}

func SetupDatabase(cfg *config.Config) *gorm.DB {
	database, err := gorm.Open(postgres.Open(MakeDSN(cfg)), &gorm.Config{})
	if err != nil {
		panic(err)
	}

	database.Exec(`CREATE EXTENSION IF NOT EXISTS "uuid-ossp";`)
	err = database.AutoMigrate(&User{}, &Post{}, &Image{}, &Comment{})

	if err != nil {
		log.Fatal(err)
	}

	return database
}
