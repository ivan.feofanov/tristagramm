package database

import (
	"time"

	"github.com/google/uuid"
)

type BaseModel struct {
	UID       uuid.UUID `gorm:"primaryKey;default:uuid_generate_v4()"`
	UpdatedAt time.Time `gorm:"index,autoUpdateTime"`
	CreatedAt time.Time `gorm:"index,autoCreateTime"`
}

type User struct {
	BaseModel
	Email    string `gorm:"index"`
	Password string
	Posts    []Post    `gorm:"constraint:OnDelete:CASCADE;"`
	Comments []Comment `gorm:"constraint:OnDelete:CASCADE;"`
}

type Post struct {
	BaseModel
	Text     string
	UserID   uuid.UUID
	Images   []Image   `gorm:"constraint:OnDelete:CASCADE;"`
	Comments []Comment `gorm:"constraint:OnDelete:CASCADE;"`
}

type Comment struct {
	BaseModel
	PostID uuid.UUID
	UserID uuid.UUID
	Text   string
}

type Image struct {
	BaseModel
	PostID uuid.UUID
	Path   string
}
